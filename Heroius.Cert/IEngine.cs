﻿using Heroius.Extension;
using System;
using System.Collections.Generic;

namespace Heroius.Cert
{
    /// <summary>
    /// 定义引擎功能接口
    /// </summary>
    public interface IEngine
    {
        void Init(Dictionary<string, string> config);

        DictEvalEntity Setting { get; }

        GenResult Gen(CertInfo info);

        event Action<string> SendMessage;
    }
}
