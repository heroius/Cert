﻿using Heroius.Extension;
using Heroius.Extension.WPF;
using PropertyChanged;
using System.Collections.Generic;

namespace Heroius.Cert
{
    [AddINotifyPropertyChangedInterface]
    public class Setting: DictEvalEntity
    {
        public Setting(Dictionary<string, string> dict) : base(dict) { }

        /// <summary>
        /// 指定所选择的证书生成程序模块，默认为 OpenSSL
        /// <para>在程序运行时，从指定的标记加载模块，实例化引擎，并在执行生成时调用引擎。</para>
        /// </summary>
        [SetValueFromDict(DefaultValue = "OpenSSL", BindToSource = true), PropertyPanel(OptionsStore = "OpenSSL\r\nBouncyCastle")]
        public string Engine { get; set; }
    }
}
