﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroius.Cert
{
    public class GenResult
    {
        public bool Succeed { get; set; } = false;

        public string Info { get; set; } = "not given";

        public readonly static GenResult SucceedResult = new GenResult() { Succeed = true, Info = "Succeed" };
    }
}
