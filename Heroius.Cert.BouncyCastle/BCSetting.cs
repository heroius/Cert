﻿using Heroius.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heroius.Cert.BouncyCastle
{
    [PropertyChanged.AddINotifyPropertyChangedInterface]
    public class BCSetting: DictEvalEntity
    {
        public BCSetting(Dictionary<string, string> dict) : base(dict) { }

        public string SetSecKey { get; set; }
        public string SetSecIV { get; set; }
    }
}
