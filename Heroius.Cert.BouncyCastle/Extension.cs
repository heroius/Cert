﻿using Org.BouncyCastle.Asn1.X509;

namespace Heroius.Cert.BouncyCastle
{
    public static class HEnhancedKeyUsageExtension
    {
        public static KeyPurposeID ToKP(this EnhancedKeyUsage usage)
        {
            switch (usage)
            {
                case EnhancedKeyUsage.AnyExtendedKeyUsage:
                    return KeyPurposeID.AnyExtendedKeyUsage;
                case EnhancedKeyUsage.ClientAuth:
                    return KeyPurposeID.IdKPClientAuth;
                case EnhancedKeyUsage.CodeSigning:
                    return KeyPurposeID.IdKPCodeSigning;
                case EnhancedKeyUsage.EmailProtection:
                    return KeyPurposeID.IdKPEmailProtection;
                case EnhancedKeyUsage.IpsecEndSystem:
                    return KeyPurposeID.IdKPIpsecEndSystem;
                case EnhancedKeyUsage.IpsecTunnel:
                    return KeyPurposeID.IdKPIpsecTunnel;
                case EnhancedKeyUsage.IpsecUser:
                    return KeyPurposeID.IdKPIpsecUser;
                case EnhancedKeyUsage.OcspSigning:
                    return KeyPurposeID.IdKPOcspSigning;
                case EnhancedKeyUsage.ServerAuth:
                    return KeyPurposeID.IdKPServerAuth;
                case EnhancedKeyUsage.SmartCardLogon:
                    return KeyPurposeID.IdKPSmartCardLogon;
                case EnhancedKeyUsage.TimeStamping:
                    return KeyPurposeID.IdKPTimeStamping;
                default:
                    throw new System.Exception("not supported enhanced key usage");
            }
        }
    }
}
