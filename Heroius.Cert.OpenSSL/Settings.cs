﻿using Heroius.Extension;
using Heroius.Extension.WPF;
using PropertyChanged;
using System.Collections.Generic;

namespace Heroius.Cert.OpenSSL
{
    [AddINotifyPropertyChangedInterface]
    public class OpenSSLSettings: DictEvalEntity
    {
        public OpenSSLSettings(Dictionary<string, string> settings): base(settings)
        {

        }

        [SetValueFromDict(DefaultValue = "C:\\OpenSSL\\bin\\openssl.exe", BindToSource = true), PropertyPanel(Type = PropertyPanelSupportedType.File)]
        public string OpenSSLPath { get; set; }
        [SetValueFromDict(DefaultValue = 500, BindToSource = true)]
        public int ProcWaitInterval { get; set; }
        [SetValueFromDict(DefaultValue = 10000, BindToSource = true)]
        public int ProcWaitLimit { get; set; }
    }
}
