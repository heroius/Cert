﻿using System;

namespace Heroius.Cert.OpenSSL
{
    public static class Extension
    {
        public static string ToProperString(this KeyUsage keyUsage)
        {
            switch (keyUsage)
            {
                case KeyUsage.EncipherOnly:
                    return "encipherOnly";
                case KeyUsage.CrlSign:
                    return "cRLSign";
                case KeyUsage.KeyCertSign:
                    return "keyCertSign";
                case KeyUsage.KeyAgreement:
                    return "keyAgreement";
                case KeyUsage.DataEncipherment:
                    return "dataEncipherment";
                case KeyUsage.KeyEncipherment:
                    return "keyEncipherment";
                case KeyUsage.NonRepudiation:
                    return "nonRepudiation";
                case KeyUsage.DigitalSignature:
                    return "digitalSignature";
                case KeyUsage.DecipherOnly:
                    return "decipherOnly";
                default:
                    throw new Exception("not supported key usage");
            }
        }

        public static string ToProperString(this EnhancedKeyUsage enhancedKeyUsage)
        {
            switch (enhancedKeyUsage)
            {
                case EnhancedKeyUsage.AnyExtendedKeyUsage:
                    return "anyExtendedKeyUsage";
                case EnhancedKeyUsage.ClientAuth:
                    return "clientAuth";
                case EnhancedKeyUsage.CodeSigning:
                    return "codeSigning";
                case EnhancedKeyUsage.EmailProtection:
                    return "emailProtection";
                case EnhancedKeyUsage.IpsecEndSystem:
                    return "ipsecEndSystem";
                case EnhancedKeyUsage.IpsecTunnel:
                    return "ipsecTunnel";
                case EnhancedKeyUsage.IpsecUser:
                    return "ipsecUser";
                case EnhancedKeyUsage.OcspSigning:
                    return "OCSPSigning";
                case EnhancedKeyUsage.ServerAuth:
                    return "serverAuth";
                case EnhancedKeyUsage.TimeStamping:
                    return "timeStamping";
                default:
                    throw new Exception("not supported enhanced key usage");
            }
        }
    }
}
